package dcxapitest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.json.simple.JSONObject;

public class LoginApiTest {
    Response response;

    //Some test cases may fail due to max attempt
    //Test case for valid credentials not having valid credentials, so just mentioning the test case

    //Test case for invalid credentials
    @Test
    public void LoginTest_TC_001(){
        RequestSpecification request= RestAssured.given();
        JSONObject json=new JSONObject();
        json.put("email","anjujangra212@gmail.com");
        json.put("password","anjujangra2128");
        json.put("purpose","authenticate");
        json.put("pe","true");
        request.header("content-type","application/json");
        request.body(json.toString());
        response=request.post("https://api.coindcx.com/api/v3/authenticate");
        System.out.println(response.asString());
        Assert.assertEquals(response.getStatusCode(),401);
        Assert.assertEquals(response.jsonPath().get("message"),"Invalid Credentials");
    }

    //Test case when email is not passed
    @Test
    public void LoginTest_TC_002(){
        RequestSpecification request= RestAssured.given();
        JSONObject json=new JSONObject();
        json.put("password","anjujangra2128");
        json.put("purpose","authenticate");
        json.put("pe","true");
        request.header("content-type","application/json");
        request.body(json.toString());
        response=request.post("https://api.coindcx.com/api/v3/authenticate");
        System.out.println(response.asString());
        Assert.assertEquals(response.getStatusCode(),422);
        Assert.assertEquals(response.jsonPath().get("message"),"\"email\" is required");
    }

    //Test case when password is not passed
    @Test
    public void LoginTest_TC_003(){
        RequestSpecification request= RestAssured.given();
        JSONObject json=new JSONObject();
        json.put("email","anjujangra21@gmail.com");
        json.put("purpose","authenticate");
        json.put("pe","true");
        request.header("content-type","application/json");
        request.body(json.toString());
        response=request.post("https://api.coindcx.com/api/v3/authenticate");
        System.out.println(response.asString());
        Assert.assertEquals(response.getStatusCode(),422);
        Assert.assertEquals(response.jsonPath().get("message"),"\"password\" is required");
    }

    //Test case when blank password is passed
    @Test
    public void LoginTest_TC_004(){
        RequestSpecification request= RestAssured.given();
        JSONObject json=new JSONObject();
        json.put("email","anjujangra2@gmail.com");
        json.put("password","");
        json.put("purpose","authenticate");
        json.put("pe","true");
        request.header("content-type","application/json");
        request.body(json.toString());
        response=request.post("https://api.coindcx.com/api/v3/authenticate");
        System.out.println(response.asString());
        Assert.assertEquals(response.getStatusCode(),422);
        Assert.assertEquals(response.jsonPath().get("message"),"Password can't be blank");
    }

    //Test case when blank email is passed
    @Test
    public void LoginTest_TC_005(){
        RequestSpecification request= RestAssured.given();
        JSONObject json=new JSONObject();
        json.put("email","");
        json.put("password","anjujangra2");
        json.put("purpose","authenticate");
        json.put("pe","true");
        request.header("content-type","application/json");
        request.body(json.toString());
        response=request.post("https://api.coindcx.com/api/v3/authenticate");
        System.out.println(response.asString());
        Assert.assertEquals(response.getStatusCode(),422);
        Assert.assertEquals(response.jsonPath().get("message"),"Email can't be blank");
    }

    //Test case when blank purpose is passed
    @Test
    public void LoginTest_TC_006(){
        RequestSpecification request= RestAssured.given();
        JSONObject json=new JSONObject();
        json.put("email","anju33jangra2@gmail.com");
        json.put("password","anjujangra2");
        json.put("purpose","");
        json.put("pe","true");
        request.header("content-type","application/json");
        request.body(json.toString());
        response=request.post("https://api.coindcx.com/api/v3/authenticate");
        System.out.println(response.asString());
        Assert.assertEquals(response.getStatusCode(),422);
        Assert.assertEquals(response.jsonPath().get("message"),"Invalid request");
    }

    //Test case when blank pe is passed
    @Test
    public void LoginTest_TC_007(){
        RequestSpecification request= RestAssured.given();
        JSONObject json=new JSONObject();
        json.put("email","anju33jangra2@gmail.com");
        json.put("password","anjujangra2");
        json.put("purpose","authenticate");
        json.put("pe","");
        request.header("content-type","application/json");
        request.body(json.toString());
        response=request.post("https://api.coindcx.com/api/v3/authenticate");
        System.out.println(response.asString());
        Assert.assertEquals(response.getStatusCode(),422);
        Assert.assertEquals(response.jsonPath().get("message"),"Invalid request");
    }

    //test cases when max attempt crossed
    @Test
    public void LoginTest_TC_008(){
        RequestSpecification request= RestAssured.given();
        JSONObject json=new JSONObject();
        json.put("email","anju33jangra2@gmail.com");
        json.put("password","anjujangra2");
        json.put("purpose","authenticate");
        json.put("pe","true");
        request.header("content-type","application/json");
        request.body(json.toString());
        request.post("https://api.coindcx.com/api/v3/authenticate");
        request.post("https://api.coindcx.com/api/v3/authenticate");
        request.post("https://api.coindcx.com/api/v3/authenticate");
        request.post("https://api.coindcx.com/api/v3/authenticate");
        request.post("https://api.coindcx.com/api/v3/authenticate");
        response=request.post("https://api.coindcx.com/api/v3/authenticate");
        System.out.println(response.asString());
        Assert.assertEquals(response.getStatusCode(),429);
        Assert.assertEquals(response.jsonPath().get("message"),"Maximum 5 attempts allowed in 5 minutes");
    }
}
